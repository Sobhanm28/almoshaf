import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'BasicData.dart' as basicData;

class ReadSura extends StatefulWidget {
  final List sura;
  final List translate;
  final String suraName;
  final ValueChanged<void> refresh;

  const ReadSura(
      {Key key, this.sura, this.translate, this.suraName, this.refresh})
      : super(key: key);

  @override
  _ReadSuraState createState() => _ReadSuraState();
}

class _ReadSuraState extends State<ReadSura> {
  double heigthSetting = 0;
  bool yelloCheckBox = false;
  bool brownCheckBox = false;
  bool blueCheckBox = false;
  bool nightCheckBox = false;
  double suraFontSize = 10;
  double translateFontSize = 10;

  width(double d) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d) {
    return MediaQuery.of(context).size.height * d;
  }

  refresh() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.suraName),
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(left: 10),
                child: IconButton(
                  onPressed: () {
                    setState(() {
                      heigthSetting = heigthSetting == 0 ? height(0.4) : 0;
                    });
                  },
                  icon: Icon(Icons.tune),
                ))
          ],
        ),
        body: Stack(
          children: <Widget>[
            Center(
              child: AnimationLimiter(
                child: ListView.separated(
                    separatorBuilder: (context, i) {
                      return Divider();
                    },
                    itemCount: widget.sura.length,
                    itemBuilder: (context, i) {
                      return AnimationConfiguration.staggeredList(
                          position: i,
                          duration: Duration(milliseconds: 375),
                          child: SlideAnimation(
                              horizontalOffset: 150.0,
                              child: FadeInAnimation(
                                  child: ListTile(
                                title: Text(
                                  "${widget.sura[i]["text"]}  ${widget.sura[i]["aya"]}",
                                  style: TextStyle(
                                      fontFamily: basicData.suraFont,
                                      fontSize: basicData.suraFontSize),
                                ),
                                subtitle: Text(
                                  "${widget.translate[i]["AyahText"]}",
                                  style: TextStyle(
                                      fontFamily: basicData.translateFont,
                                      fontSize: basicData.translateFontSize),
                                ),
                              ))));
                    }),
              ),
            ),
            Positioned(
                bottom: 0,
                child: AnimatedContainer(
                  duration: Duration(milliseconds: 300),
                  curve: Curves.easeInCubic,
                  width: width(1),
                  height: heigthSetting,
                  color: Theme.of(context).accentColor,
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Text(
                          "انتخاب تم",
                          style: TextStyle(
                              fontWeight: FontWeight.w900, color: Colors.white),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                basicData.mtColor = Colors.brown;
                                widget.refresh(null);
                              },
                              child: Text(
                                "قهوه ای",
                                style: TextStyle(
                                    fontSize: 13, color: Colors.white),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                basicData.mtColor = Colors.blueGrey;
                                widget.refresh(null);
                              },
                              child: Text(
                                "تیره",
                                style: TextStyle(
                                    fontSize: 13, color: Colors.white),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                basicData.mtColor = Colors.blue;
                                widget.refresh(null);
                              },
                              child: Text(
                                "آبی",
                                style: TextStyle(
                                    fontSize: 13, color: Colors.white),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                basicData.darkMode = !basicData.darkMode;
                                widget.refresh(null);
                              },
                              child: Text(
                                basicData.darkMode ? "روز" : "شب",
                                style: TextStyle(
                                    fontSize: 13, color: Colors.white),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          height: height(0.02),
                        ),
                        Text(
                          "انتخاب فونت آیه",
                          style: TextStyle(
                              fontWeight: FontWeight.w900, color: Colors.white),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            GestureDetector(
                                onTap: () {
                                  basicData.suraFont = "thols";
                                  widget.refresh(null);
                                },
                                child: Text("ثلث",
                                    style: TextStyle(
                                        fontSize: 13, color: Colors.white))),
                            GestureDetector(
                                onTap: () {
                                  basicData.suraFont = "nabi";
                                  widget.refresh(null);
                                },
                                child: Text("نبی",
                                    style: TextStyle(
                                        fontSize: 13, color: Colors.white))),
                            GestureDetector(
                                onTap: () {
                                  basicData.suraFont = "taha";
                                  widget.refresh(null);
                                },
                                child: Text("طاها",
                                    style: TextStyle(
                                        fontSize: 13, color: Colors.white))),
                          ],
                        ),
                        Container(
                          height: height(0.02),
                        ),
                        Text(
                          "انتخاب فونت ترجمه",
                          style: TextStyle(
                              fontWeight: FontWeight.w900, color: Colors.white),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                basicData.translateFont = "IRANSans";
                                widget.refresh(null);
                              },
                              child: Text("ایران سنس",
                                  style: TextStyle(
                                      fontSize: 13, color: Colors.white)),
                            ),
                            GestureDetector(
                              onTap: () {
                                basicData.translateFont = "Shabnam";
                                widget.refresh(null);
                              },
                              child: Text("شبنم",
                                  style: TextStyle(
                                      fontSize: 13, color: Colors.white)),
                            ),
                          ],
                        ),
                        Container(
                          height: height(0.02),
                        ),
                        Text(
                          "انتخاب سایز سوره",
                          style: TextStyle(
                              fontWeight: FontWeight.w900, color: Colors.white),
                        ),
                        Container(
                          padding: EdgeInsets.all(5),
                          child: CupertinoSlider(
                            onChanged: (v) {
                              basicData.suraFontSize = v;
                              widget.refresh(null);
                              print(v);
                            },
                            activeColor: Colors.red,
                            max: 30,
                            min: 16,
                            value: basicData.suraFontSize,
                            divisions: 14,
                          ),
                        ),
                        Container(
                          height: height(0.02),
                        ),
                        Text(
                          "انتخاب سایز ترجمه",
                          style: TextStyle(
                              fontWeight: FontWeight.w900, color: Colors.white),
                        ),
                        Container(
                          padding: EdgeInsets.all(5),
                          child: CupertinoSlider(
                            onChanged: (v) {
                              basicData.translateFontSize = v;
                              widget.refresh(null);
                              print(v);
                            },
                            activeColor: Colors.red,
                            max: 30,
                            min: 14,
                            value: basicData.translateFontSize,
                            divisions: 16,
                          ),
                        )
                      ],
                    ),
                  ),
                ))
          ],
        ));
  }
}
