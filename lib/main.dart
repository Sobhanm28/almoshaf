import 'dart:io';

import 'package:almoshaf/ReadSura.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:path/path.dart' as prefixPath;
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import "BasicData.dart" as basicData;

MaterialColor mtColor = Colors.blue;

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  refresh(_) {
    print("theme Changed");
    setState(() {
      
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      localizationsDelegates: [
        // ... app-specific localization delegate[s] here
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('fa'), // farsi
      ],
      theme: ThemeData(
          fontFamily: "nabi",
          primarySwatch: basicData.mtColor,
          brightness:basicData.darkMode?Brightness.dark : Brightness.light),
      home: MyHomePage(
        refresh: refresh,
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final ValueChanged<void> refresh;

  const MyHomePage({Key key, this.refresh}) : super(key: key);
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Database database;
  List suraNames = [];
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  var switcherWidget;
  var appbarSwitcher;
  TextEditingController searchController;

  width(double d) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d) {
    return MediaQuery.of(context).size.height * d;
  }

  @override
  void dispose() {
    super.dispose();
    searchController.dispose();
  }

  @override
  void initState() {
    super.initState();
    initDB();
    searchController = TextEditingController();
  }

  initDB() async {
    print("start intialize DB");

    Future.delayed(Duration.zero, () {
      setState(() {
        switcherWidget = switchToProgressbar();
        appbarSwitcher = switchToAppbar();
      });
    });

    Directory directory = await getExternalStorageDirectory();
    String path = prefixPath.join(directory.path, "QuranDB");
    if (database == null) {
      ByteData data = await rootBundle.load("assets/database/testDB");
      List<int> bytes =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);

      await File(path).writeAsBytes(bytes);

      database = await openDatabase(path);
    } else {
      database = await openDatabase(path);
    }

    suraNames = await database.query("tbl_suraName");
    setState(() {
      switcherWidget = switchToSuraList();
    });
  }

  Future<List> readSuraTrFromId(String suraId) async {
    Directory directory = await getExternalStorageDirectory();
    String path = prefixPath.join(directory.path, "QuranDB");
    Database database = await openDatabase(path);
    List suraAya = await database.rawQuery(
        "SELECT * FROM tbl_suraName INNER JOIN quran_text ON tbl_suraName.id=quran_text.sura WHERE tbl_suraName.id=?",
        [suraId]);
    List suraTranslate = await database.rawQuery(
        "SELECT * FROM tbl_suraName INNER JOIN Quran ON tbl_suraName.id=Quran.SuraId WHERE tbl_suraName.id=?",
        [suraId]);
    return [suraAya, suraTranslate];
  }

  setTheme() {}

  switchToSuraList() {
    return AnimationLimiter(
      child: ListView.separated(
          separatorBuilder: (context, i) => Divider(),
          itemCount: suraNames.length,
          itemBuilder: (context, i) {
            return AnimationConfiguration.staggeredList(
              position: i,
              duration: Duration(milliseconds: 200),
              child: SlideAnimation(
                horizontalOffset: 80.0,
                child: GestureDetector(
                  onTap: () async {
                    String suraId = suraNames[i]["id"].toString();
                    readSuraTrFromId(suraId).then((lists) => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ReadSura(
                            suraName: suraNames[i]["sura"],
                            sura: lists[0],
                            translate: lists[1],
                            refresh: widget.refresh,
                          ),
                        )));
                  },
                  child: Container(
                    padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "${suraNames[i]["id"]}",
                          style: TextStyle(fontFamily: "thols", fontSize: 25),
                        ),
                        Text(
                          suraNames[i]["sura"],
                          style: TextStyle(fontSize: 20),
                        ),
                        Text("")
                      ],
                    ),
                  ),
                ),
              ),
            );
          }),
    );
  }

  switchToProgressbar() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  switchToSearchBox() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Expanded(
            flex: 1,
            child: IconButton(onPressed: () {}, icon: Icon(Icons.search))),
        Expanded(
          flex: 5,
          child: Container(
              alignment: Alignment.center,
              width: width(1),
              child: TextField(
                controller: searchController,
                onChanged: (v) {
                  print(v);
                  setState(() {
                    switcherWidget = switchToProgressbar();
                  });
                  searchDB(v);
                },
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white, fontSize: 16),
                decoration: InputDecoration(
                  hintStyle: TextStyle(color: Colors.white),
                  hintText: "جست و جوی سوره ها",
                ),
              )),
        ),
        Expanded(
            flex: 1,
            child: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  searchController.clear();
                  initDB();
                  setState(() {
                    appbarSwitcher = switchToAppbar();
                  });
                }))
      ],
    );
  }

  switchToAppbar() {
    return Row(
      children: <Widget>[
        Expanded(
            flex: 1,
            child: IconButton(
                onPressed: () {
                  _scaffoldKey.currentState.openDrawer();
                },
                icon: Icon(Icons.menu))),
        Expanded(
          flex: 5,
          child: Container(
              alignment: Alignment.center,
              width: width(1),
              child: Text(
                "المصحف الشریف",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 22),
              )),
        ),
        Expanded(
            flex: 1,
            child: IconButton(
                icon: Icon(Icons.search),
                onPressed: () {
                  setState(() {
                    appbarSwitcher = switchToSearchBox();
                  });
                }))
      ],
    );
  }

  emptyList() {
    return Text(
      "سوره مورد نظر یافت نشد \n لطفا پس از بررسی مجددا امتحان نمایید",
      textAlign: TextAlign.center,
    );
  }

  searchDB(String v) async {
    Database db = await database;
    String sql = '''
    SELECT * FROM tbl_suraName WHERE sura LIKE ?
    ''';
    suraNames = await db.rawQuery(sql, ["%$v%"]);
    if (suraNames.isNotEmpty) {
      setState(() {
        switcherWidget = switchToSuraList();
      });
    } else {
      setState(() {
        switcherWidget = emptyList();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        actions: <Widget>[
          Expanded(
            child: AnimatedSwitcher(
              duration: Duration(seconds: 3),
              child: appbarSwitcher,
            ),
          )
        ],
      ),
      drawer: SizedBox(
        width: width(0.75),
        height: height(1),
        child: Drawer(
          child: Container(
            alignment: Alignment.center,
            child: Text("in the name of god"),
          ),
        ),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Center(
                child: GestureDetector(
                  onTap: () {
                    basicData.mtColor = Colors.purple;
                    widget.refresh(null);
                  },
                  child: Text(
                    "فهرست سوره ها",
                    style: TextStyle(fontSize: 18),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 17,
              child: AnimatedSwitcher(
                duration: Duration(milliseconds: 800),
                child: switcherWidget,
              ),
            )
          ],
        ),
      ),
    );
  }
}
